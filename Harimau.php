<?php
/**
 *
 */
class Harimau
{
    use Hewan,Fight;

    public function __construct($name)
    {
      $this->name = $name;
      $this->legs  = 4;
      $this->skill = "lari cepat";
      $this->attackPower = 7 ;
      $this->defencePower = 8 ;

    }

    public function getInfoHewan()
    {
        return "Nama : " . $this->name .
        "<br>HP : " . $this->hp .
        "<br>Kaki : " . $this->legs .
        "<br>Skill : " . $this->skill .
        "<br>Attack Power : " .$this->attackPower .
        "<br>Defence Power : " .$this->defencePower;
    }

}

 ?>
