<?php
/**
 *
 */
class Elang {
    use Hewan,Fight;

    public function __construct($name)
    {
      $this->name = $name;
      $this->legs  = 2;
      $this->skill = "terbang tinggi";
      $this->attackPower = 10 ;
      $this->defencePower = 5 ;

    }
    public function getInfoHewan()
    {
        return "Nama : " . $this->name .
        "<br>HP : " . $this->hp .
        "<br>Kaki : " . $this->legs .
        "<br>Skill : " . $this->skill .
        "<br>Attack Power : " .$this->attackPower .
        "<br>Defence Power : " .$this->defencePower;
    }

}


 ?>
